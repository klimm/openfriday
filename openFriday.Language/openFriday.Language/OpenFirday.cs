using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using Xunit;
using System.Diagnostics;

namespace openFriday.Language
{
    public class OpenFirday
    {
        [Fact]
        public void ObjectArrayCollectionPropertyInitializer()
        {
            var kalleJansen = new Person { Firstname = "Kalle", Lastname = "Jansen" };

            var personCollection = new Collection<Person> { new Person(), kalleJansen, new Person() };

            var personArray = new[] { new Person(), new Person(), kalleJansen };

            var personDictionary = new Dictionary<int, Person>
            {
                {0, new Person()},
                {1, new Person
                    {
                        Firstname = "Stefan",
                        Lastname = "Hansen",
                        Address = new Address
                        {
                            Street = "Somewhere 42",
                            City = "Hamburg"
                        }
                    }
                },
                {2, kalleJansen}
            };
        }

        [Fact]
        public void Show()
        {
            var person = new Person {Address = new Address {Street = null}};
            person.Foo();

            person.Bar();

            person.FooBar();

            person.MiauWauWau();
        }

        [Fact]
        public void TypePatterns_And_LocalFunctions()
        {
            var closure = 1;

            void localFunction(object input)
            {
                if (input is int count)
                    Assert.Equal(42 + 1, count + closure);

                if (input is string text)
                    Assert.Equal(5, text.Length + closure);
            }

            localFunction(42);
            localFunction("test");
            localFunction(true);
        }

        [Fact]
        public void PatternMatching()
        {
            string ageBlock = string.Empty;
            int age = 82;
            //int age = 94;
            //int age = 100;

            switch (age)
            {
                case 50:
                    ageBlock = "the big five-oh";
                    break;
                case int testAge when new[] { 80, 81, 82, 83, 84, 85, 86, 87, 88, 89 }.Contains(testAge):
                    ageBlock = $"octogenarian {age}";
                    break;
                case var _ when (age >= 90) & (age <= 99):
                    ageBlock = "nonagenarian";
                    break;
                case var testAge when testAge >= 100:
                    ageBlock = "centenarian";
                    break;
                default:
                    ageBlock = "just old";
                    break;
            }

            Console.WriteLine(ageBlock);
        }

        [Fact]
        public void AnonymousTypes()
        {
            var anonym = new { Title = "Mr.", Name = "X", Sex = 'm' };

            Assert.Equal('m', anonym.Sex);
            
            //anonym.Name = "Peter"; <- NoSetter Immutable

            var anonym2 = Enumerable.Range(0, 42).Select(val => new { Value = val, Pow = Math.Pow(val, 2) }).Last();
           
            Trace.WriteLine($"{nameof(anonym2.Value)}:{anonym2.Value} - {nameof(anonym2.Pow)}:{anonym2.Pow}");
        }

        [Fact]
        public void TupleLiterals()
        {
            var returnTuple = this.FooBar((42, "84", true));

            Assert.Equal(84, returnTuple.IntValue);
            Assert.Equal("42", returnTuple.StrValue);
        }

        private (int IntValue, string StrValue) FooBar((int intValue, string stringValue, bool auto) myTuple)
        {
            var asString = myTuple.intValue.ToString();
            var asInt = int.Parse(myTuple.stringValue);
            return (asInt, asString);
        }

        [Fact]
        public void Yield()
        {
            foreach (var val in this.Read("").AsParallel())
                Trace.WriteLine(val);
        }

        public IEnumerable<object[]> Read(string sql)
        {
            using (var connection = new SqlConnection())
            using (var command = new SqlCommand(sql, connection))
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    var row = new object[reader.FieldCount];
                    reader.GetValues(row);
                    yield return row;
                }
            }
            yield return new object[1];
        }

        private IEnumerable Yielder(bool withThree)
        {
            yield return 1;
            yield return 2;
            if (withThree)
                yield return 3;
            yield return 4;
        }
    }
}
