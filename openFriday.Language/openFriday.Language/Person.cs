﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace openFriday.Language
{
    class Person
    {
        public virtual string Firstname { get; set; } = "Max";

        public string Lastname { get; set; } = "Mustermann";

        public string Fullname => $"{this.Firstname ?? $"test {this.Address.City}"} {this.Lastname}";

        public Address Address { get; set; } = new Address {City = "Hamburg", Street = "Sonninstraße 28"};

        public void Foo()
        {
            if (this.Address?.Street?.Length < 5)
                throw new NotSupportedException($"Your {nameof(this.Address.Street)} is to short ;)");
        }

        public string Bar()
        {
            return this.Address?.Street ?? "Standardstraße";
        }

        public int FooBar()
        {
            return this.Firstname == "Max" ? 42 : 84;
        }

        public void MiauWauWau(
            [CallerMemberName] string callerName = null, 
            [CallerFilePath] string callerFilePath = null, 
            [CallerLineNumber] int callerLineNumber = default(int))
        {
            Trace.Write($"{callerLineNumber}. {callerFilePath} - {callerName}");
        }
    }

    //Test
    class Address
    {
        public string City { get; set; } = "Freiburg";
        public string Street { get; set; } = "Klingelgasse";
    }
}
