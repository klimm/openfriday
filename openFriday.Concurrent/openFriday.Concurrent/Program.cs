﻿using System;
using System.Threading.Tasks;

namespace openFriday.Concurrent
{
	class Program
	{
		static void Main(string[] args)
		{
			//new Theard_Task().Run();

			new Parallel_Sync().Run();

			// ---

			//new AsyncAwait().Run().Wait();

			//new PraxisWithDb().Run();
			Console.ReadKey();
		}
	}
}
