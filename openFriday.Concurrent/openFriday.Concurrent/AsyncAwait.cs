﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace openFriday.Concurrent
{
	class AsyncAwait
	{
		Log log;
		readonly HttpClient client;
		public AsyncAwait()
		{
			this.client = new HttpClient();
		}

		internal async Task Run()
		{
			using (this.log = new Log())
			{
				var tasks = new Collection<Task>();
				Enumerable.Range(0, 10).ForEach(idx =>
				{
					Task<byte[]> downloadTask = this.DownloadAsync("http://ipv4.download.thinkbroadband.com/10MB.zip");
					Task saveTask = this.SaveAsync($@"C:\_myTrash\of\{idx}.zip", downloadTask);
					
					tasks.Add(saveTask);
				});
				
				await Task.WhenAll(tasks);
				this.log.Done("ALL downloaded and saved");
			}
		}

		private async Task SaveAsync(string path, Task<byte[]> downloadTask)
		{
			File.WriteAllBytes(path, await downloadTask);
			this.log.Done($"{path} downloaded and saved");
		}

		private Task<byte[]> DownloadAsync(string location)
		{
			return this.client.GetByteArrayAsync(location);
		}
	}
}