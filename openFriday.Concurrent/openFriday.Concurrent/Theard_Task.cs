﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace openFriday.Concurrent
{
	class Theard_Task
	{
		internal void Run()
		{
			new Threading().Run();
			Console.WriteLine(new string('-', 80));
			new Tasking().Run();
		}
	}

	class Threading
	{
		Log log;
		ManualResetEvent syncEvent1;
		ManualResetEvent syncEvent2;

		public void Run()
		{
			using (this.log = new Log())
			{
				this.syncEvent1 = new ManualResetEvent(false);
				this.syncEvent2 = new ManualResetEvent(false);

				var thread1 = new Thread(this.Method1);
				var thread2 = new Thread(this.Method2);

				thread1.Start();
				thread2.Start("string parameterized thread...");

				WaitHandle.WaitAll(new[] {this.syncEvent1, this.syncEvent2 });
			}
		}

		private void Method1()
		{
			this.log.Start("T1");
			Thread.Sleep(TimeSpan.FromSeconds(5));
			this.log.Done("T1");
			this.syncEvent1.Set();
		}

		private void Method2(object obj)
		{
			var data = (string)obj;
			this.log.Start("T2", data);
			Thread.Sleep(TimeSpan.FromSeconds(3));
			this.log.Done("T2", data);
			this.syncEvent2.Set();
		}
	}

	class Tasking
	{
		Log log;
		public void Run()
		{
			using (this.log = new Log())
			{
				var task1 = Task.Run(() => this.Method1());
				var task2 = Task.Run(() => this.Method2("string parameterized thread..."));

				Task.WhenAll(task1, task2).Wait();
			}
		}

		private void Method1()
		{
			this.log.Start("T1");
			Task.Delay(TimeSpan.FromSeconds(5)).Wait();
			this.log.Done("T1");
		}

		private void Method2(string data)
		{
			this.log.Start("T2", data);
			Task.Delay(TimeSpan.FromSeconds(3)).Wait();
			this.log.Done("T2", data);
		}
	}
}
