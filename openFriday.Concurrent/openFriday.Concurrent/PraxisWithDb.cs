﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using Dapper;
using Oracle.ManagedDataAccess.Client;

namespace openFriday.Concurrent
{
	class PraxisWithDb
	{
		internal void Run()
		{
			using (OracleConnection con = new OracleConnection("connStr"))
			{
				con.Open();
				IDataReader reader = con.ExecuteReader("select * from ENUM");
				this.ProcessEnumsParallel(this.Read(reader));
			}
			
			Console.WriteLine(new string('-', 80));
			
			using (OracleConnection con = new OracleConnection("connStr"))
			{
				con.Open();
				IDataReader reader = con.ExecuteReader("select * from ENUM");
				this.ProcessEnums(this.Read(reader));
			}
		}
		
		IEnumerable<MyEnum> Read(IDataReader reader)
		{
			while (reader.Read())
				yield return new MyEnum { Id = reader.GetInt64(0), Text = reader.GetString(3) };
		}

		void ProcessEnumsParallel(IEnumerable<MyEnum> myEnums)
		{
			using (new Log())
			{
				myEnums.AsParallel()
						.WithDegreeOfParallelism(4) // <------------------------------------------------
						.ForAll(x => { Thread.Sleep(1); }); //Simulation von arbeit
			}
		}

		void ProcessEnums(IEnumerable<MyEnum> myEnums)
		{
			using (new Log())
			{
				myEnums.ForEach(x => { Thread.Sleep(1); }); //Simulation von arbeit
			}
		}

		private class MyEnum
		{
			public Int64 Id { get; set; }
			public string Text { get; set; }
		}
	}
}