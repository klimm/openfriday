﻿using System;
using System.IO;
using System.Linq;
using System.Threading;

namespace openFriday.Concurrent
{
	class Parallel_Sync
	{
		readonly string[] fileExtensions = { ".cs", ".js", ".json", ".xml" };

		internal void Run()
		{
			this.RunSync();
			Console.WriteLine(new string('-', 80));
			this.RunPara();
		}

		void RunSync()
		{
			using (new Log())
			{
				long size = 0;
				int count = 0;
				Directory.EnumerateFiles(@"C:\_Daten", "*.*", SearchOption.AllDirectories)
					.Select(filePath => new FileInfo(filePath))
					.Where(fileInfo => this.fileExtensions.Contains(fileInfo.Extension))
					.ForEach(fileInfo =>
					{
						//Console.Write(Thread.CurrentThread.ManagedThreadId);
						count++;
						size += fileInfo.Length;
					});

				Console.WriteLine($"{count} files with a size of {size >> 20} MB");
			}
		}

		void RunPara()
		{
			using (new Log())
			{
				long size = 0;
				int count = 0;
				Directory.EnumerateFiles(@"C:\_Daten", "*.*", SearchOption.AllDirectories)
					.AsParallel()
					.Select(filePath => new FileInfo(filePath))
					.Where(fileInfo => this.fileExtensions.Contains(fileInfo.Extension))
					.ForAll(fileInfo =>
					{
						//Console.Write(Thread.CurrentThread.ManagedThreadId);
						Interlocked.Increment(ref count);
						Interlocked.Add(ref size, fileInfo.Length);
					});

				Console.WriteLine($"{count} files with a size of {size >> 20} MB");
			}
		}
	}
}