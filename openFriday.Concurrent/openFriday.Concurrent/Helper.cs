﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;

namespace openFriday.Concurrent
{
	class Log : IDisposable
	{
		readonly Stopwatch watch;
		readonly string caller;

		public Log([CallerFilePath] string callerPath = null, [CallerMemberName] string memberName = null)
		{
			this.watch = Stopwatch.StartNew();
			this.caller = $"{Path.GetFileNameWithoutExtension(callerPath)}.{memberName}";
			Console.WriteLine($"{this.caller}\t\tThread-Id: {Thread.CurrentThread.ManagedThreadId}");
		}

		internal void Start(string content = null, string data = null)
		{
			Console.WriteLine($"Start: {content} \t\tThread-Id: {Thread.CurrentThread.ManagedThreadId}\t\t\t\t{data}");
		}

		internal void Done(string content = null, string data = null)
		{
			Console.WriteLine($" Done: {content}\t\tThread-Id: {Thread.CurrentThread.ManagedThreadId}\t{this.watch.Elapsed:G} \t{data}");
		}

		void IDisposable.Dispose()
		{
			Console.WriteLine($"{this.caller}\t\tThread-Id: {Thread.CurrentThread.ManagedThreadId}\t{this.watch.Elapsed:G}");
		}
	}

	static class Extensions
	{
		internal static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
		{
			foreach (var current in enumerable)
				action(current);
		}
	}
}
