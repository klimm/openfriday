namespace DownloadUi
{
	partial class DownloadForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnAsync = new System.Windows.Forms.Button();
			this.btnSync = new System.Windows.Forms.Button();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.label1 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnAsync
			// 
			this.btnAsync.Location = new System.Drawing.Point(610, 187);
			this.btnAsync.Name = "btnAsync";
			this.btnAsync.Size = new System.Drawing.Size(75, 23);
			this.btnAsync.TabIndex = 0;
			this.btnAsync.Text = "AsyncAwait";
			this.btnAsync.UseVisualStyleBackColor = true;
			this.btnAsync.Click += new System.EventHandler(this.btnAsync_Click);
			// 
			// btnSync
			// 
			this.btnSync.Location = new System.Drawing.Point(55, 187);
			this.btnSync.Name = "btnSync";
			this.btnSync.Size = new System.Drawing.Size(75, 23);
			this.btnSync.TabIndex = 1;
			this.btnSync.Text = "Sync";
			this.btnSync.UseVisualStyleBackColor = true;
			this.btnSync.Click += new System.EventHandler(this.btnSync_Click);
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(55, 88);
			this.progressBar1.MarqueeAnimationSpeed = 10;
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(630, 84);
			this.progressBar1.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(52, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(347, 55);
			this.label1.TabIndex = 8;
			this.label1.Text = "Status";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(324, 187);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 9;
			this.button1.Text = "Task";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.btnAsyncTask_Click);
			// 
			// DownloadForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(720, 242);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.progressBar1);
			this.Controls.Add(this.btnSync);
			this.Controls.Add(this.btnAsync);
			this.Name = "DownloadForm";
			this.Text = "Downloader";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnAsync;
		private System.Windows.Forms.Button btnSync;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button button1;
	}
}

