using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DownloadUi
{
	public partial class DownloadForm : Form
	{
		public DownloadForm()
		{
			InitializeComponent();
			this.progressBar1.Maximum = 100;
		}

		private void btnSync_Click(object sender, EventArgs e)
		{
			var client = new WebClient();
			this.label1.Text = "Start";
			client.DownloadFile(new Uri(@"http://ipv4.download.thinkbroadband.com/100MB.zip"), @"c:\_Daten\bla.zip");
			this.label1.Text = "Finished";
		}

		private void btnAsyncTask_Click(object sender, EventArgs e)
		{
			var client = new WebClient();

			this.progressBar1.Style = ProgressBarStyle.Marquee;
			this.label1.Text = "Start";
			Task t = Task.Run(() => client.DownloadFile(new Uri(@"http://ipv4.download.thinkbroadband.com/100MB.zip"), @"c:\_Daten\bla.zip"));
			t.ContinueWith(t1 =>
			{
				this.label1.Text = "Finished";
				this.progressBar1.Style = ProgressBarStyle.Blocks;

			}, CancellationToken.None, TaskContinuationOptions.OnlyOnRanToCompletion, TaskScheduler.FromCurrentSynchronizationContext());
		}

		private  void btnAsync_Click(object sender, EventArgs e)
		{
			Task.Run(() => throw new IndexOutOfRangeException("Tobi"));


			//var client = new WebClient();
			//client.DownloadProgressChanged += (s, e1) => this.progressBar1.Value = e1.ProgressPercentage;

			//this.label1.Text = "Start";
			//client.DownloadFileTaskAsync(new Uri(@"http://ipv4.download.thinkbroadband.com/100MB.zip"),@"c:\_Daten\bla.zip");
			//this.label1.Text = "Finished";
		}
	}
}
