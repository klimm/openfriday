using System;
using System.Diagnostics;
using openFriday.DI;
using Xunit;

namespace openFirday.DI.Test
{
	public class UnitTest
	{
		[Fact]
		public void TestBombe()
		{
			var kernel = Werkstatt.Ninject();
			kernel.Rebind<IGrabable>().To<TestBombe>();

			Program.Run(kernel);
		}
	}

	public class TestBombe : IGrabable
	{
		public void Buddel()
		{
			Trace.WriteLine("BOOM - gro�es loch");			
		}
	}
}
