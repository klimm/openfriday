﻿using System;

namespace openFriday.DI
{
	public class Lehrling
	{
		private readonly IGrabable grabeWerkzeug;

		public Lehrling(IGrabable grabeWerkzeug)
		{
			this.grabeWerkzeug = grabeWerkzeug;
		}

		public void GrabeLoch()
		{
			this.grabeWerkzeug.Buddel();
		}
	}
}