﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace openFriday.DI
{
	public class Program
	{
		static void Main(string[] args)
		{
			//var services = Werkstatt.Microsoft();
			var services = Werkstatt.Ninject();
			
			Run(services);
			Console.Read();
		}

		public static void Run(IServiceProvider services)
		{
			var meister = services.GetService<Meister>();
			meister.GibtAnweisung();
		}
	}
}
