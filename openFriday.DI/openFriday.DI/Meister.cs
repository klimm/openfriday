﻿using System;

namespace openFriday.DI
{
	public class Meister
	{
		private readonly Lehrling lehrling;

		/// <inheritdoc />
		public Meister(Lehrling lehrling)
		{
			this.lehrling = lehrling;
		}

		public void GibtAnweisung()
		{
			this.lehrling.GrabeLoch();
		}
	}
}