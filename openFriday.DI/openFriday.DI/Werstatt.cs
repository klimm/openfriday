﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Ninject;

namespace openFriday.DI
{
	public class Werkstatt
	{
		public static IKernel Ninject()
		{
			IKernel kernel = new StandardKernel();
			kernel.Bind<Meister>().ToSelf().InSingletonScope();

			kernel.Bind<Lehrling>().ToSelf().InTransientScope();
			kernel.Bind<IGrabable>().To<Bagger>().InTransientScope();
			return kernel;
		}

		public static IServiceProvider Microsoft()
		{
			var services = new ServiceCollection();

			services.AddSingleton<Meister>();
			services.AddTransient<Lehrling>();
			services.AddTransient<IGrabable, Schaufel>();

			return services.BuildServiceProvider();
		}
	}
}