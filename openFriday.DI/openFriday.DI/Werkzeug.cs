﻿using System;

namespace openFriday.DI
{
	public interface IGrabable
	{
		void Buddel();
	}

	public class Schaufel : IGrabable
	{
		void IGrabable.Buddel()
		{
			Console.WriteLine("Loch wird geschaufelt");
		}
	}

	public class Spaten : IGrabable
	{
		/// <inheritdoc />
		public void Buddel()
		{
			Console.WriteLine("Loch wird gestochen");
		}
	}

	public class Bagger : IGrabable
	{
		/// <inheritdoc />
		public void Buddel()
		{
			Console.WriteLine("Loch wird gebaggert");
		}
	}
}