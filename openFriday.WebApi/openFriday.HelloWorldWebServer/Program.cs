﻿using System;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace openFriday.HelloWorldWebServer
{
	public class Program
	{
		public static void Main(string[] args)
		{
			WebHost.CreateDefaultBuilder(args)
				.Configure(app => app.Run(handler => handler.Response.WriteAsync("Hello OpenFriday")))
				.Build()
				.Run();
		}
	}
}
