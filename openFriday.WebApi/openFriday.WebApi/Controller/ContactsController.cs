﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using openFriday.WebApi.Contracts;
using openFriday.WebApi.Models;

namespace openFriday.WebApi.Controller
{
	[Route("api/[controller]")]
	public class ContactsController : ControllerBase
	{
		private readonly IRepository<Contact> repository;

		public ContactsController(IRepository<Contact> repository)
		{
			this.repository = repository;
		}

		[HttpGet]
		public ActionResult<IEnumerable<Contact>> Get()
		{
			var contacts = this.repository.All();
			if (!contacts.Any())
				return this.NoContent();

			return this.Ok(contacts);
		}

		[HttpGet("{id}")]
		public ActionResult<Contact> Get(int id)
		{
			var contact = this.repository.Get(id);
			if (contact == null)
				return this.NotFound(id);

			return this.Ok(contact);
		}

		[HttpPost]
		public ActionResult<Contact> Add([FromBody]Contact contact)
		{
			if (this.repository.Get(contact.Id) != null)
				return this.Conflict(contact.Id);

			return this.Created($"contacts/{contact.Id}", this.repository.Add(contact));
		}

		[HttpDelete("{id}")]
		public ActionResult<Contact> Remove(int id)
		{
			if (this.repository.Get(id) == null)
				return this.Conflict(id);

			this.repository.Remove(id);

			return this.NoContent();
		}
	}
}