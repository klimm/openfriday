﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using openFriday.WebApi.Contracts;
using openFriday.WebApi.Models;

namespace openFriday.WebApi.Domain
{
	public class ContactRepository : IRepository<Contact>
	{
		private static readonly ICollection<Contact> data = new Collection<Contact>
		{
			new Contact { Id = 0, Firstname = "Steve " , Lastname = "Jobs", Age = 56},
			new Contact { Id = 1, Firstname = "Bill " , Lastname = "Gates", Age = 62},
			new Contact { Id = 2, Firstname = "Konrad " , Lastname = "Zuse", Age = 85},
			new Contact { Id = 3, Firstname = "Niklaus" , Lastname = "Wirth", Age = 84},
		};

		/// <inheritdoc />
		public Contact Get(int id)
		{
			return data.SingleOrDefault(c => c.Id == id);
		}

		/// <inheritdoc />
		public IEnumerable<Contact> All()
		{
			return data.ToList();
		}

		/// <inheritdoc />
		public Contact Add(Contact contact)
		{
			data.Add(contact);
			return contact;
		}

		/// <inheritdoc />
		public void Remove(int id)
		{
			data.Remove(this.Get(id));
		}
	}
}