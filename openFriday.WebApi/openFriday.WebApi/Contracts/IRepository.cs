﻿using System;
using System.Collections.Generic;

namespace openFriday.WebApi.Contracts
{
	public interface IRepository<T>
	{
		T Get(int id);
		IEnumerable<T> All();
		T Add(T contact);
		void Remove(int id);
	}
}