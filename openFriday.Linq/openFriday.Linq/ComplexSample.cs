﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace openFriday.Linq
{
    public static class ComplexSample
    {
        internal static void PrintStats(Statistics stats)
        {
            Console.WriteLine($"Statistiken der Zehn größten Datein im Ordner: {stats.Path} von letzter Woche auf die zuletzt zugegriffen wurden:");
            Console.WriteLine($"Zusammen: {stats.TotalSize / 1024:n0} KB");
            Console.WriteLine($"Größte: {stats.MaxFileSize / 1024:n0} KB");
            Console.WriteLine($"Kleinste: {stats.MinFileSize / 1024:n0} KB");
            Console.WriteLine($"Durchschnittsgröße: {stats.AverageFileSize / 1024:n0} KB");
            Console.ReadLine();
        }

        internal static Statistics GatherStatsNormal(string folderPath)
        {
            var statistics = new Statistics { Path = folderPath };

            string[] filePaths = Directory.GetFiles(folderPath);
            var lastWeekFiles = new List<FileInfo>();
            foreach (var fi in filePaths.Select(p => new FileInfo(p)))
            {
                if (fi.CreationTime < DateTime.Now.AddDays(-7))
                    lastWeekFiles.Add(fi);
            }

            lastWeekFiles.Sort((x, y) => x.LastAccessTime.CompareTo(y.LastAccessTime));

            var largestLastWeekFiles = lastWeekFiles.GetRange(lastWeekFiles.Count - 10, 10);

            statistics.MinFileSize = long.MaxValue;
            foreach (var fileInfo in largestLastWeekFiles)
            {
                if (fileInfo.Length > statistics.MaxFileSize)
                    statistics.MaxFileSize = fileInfo.Length;

                if (fileInfo.Length < statistics.MinFileSize)
                    statistics.MinFileSize = fileInfo.Length;

                statistics.TotalSize += fileInfo.Length;
            }

            statistics.AverageFileSize = statistics.TotalSize / (double)largestLastWeekFiles.Count;
            return statistics;
        }

        internal static Statistics GatherStatsLinq(string folderPath)
        {
            var x = (from path in Directory.GetFiles(folderPath)
                let fi = new FileInfo(path)
                where fi.CreationTime < DateTime.Now.AddDays(-7)
                orderby fi.LastAccessTime
                select fi.Length)
                .TakeLast(10);

            var sizes = Directory.GetFiles(folderPath)
                .Select(p => new FileInfo(p))
                .Where(fi => fi.CreationTime < DateTime.Now.AddDays(-7))
                .OrderBy(fi => fi.LastAccessTime)
                .Select(fi => fi.Length)
                .TakeLast(10);
           
            return new Statistics
            {
                Path = folderPath,
                MinFileSize = sizes.Min(),
                MaxFileSize = sizes.Max(),
                AverageFileSize = sizes.Average(),
                TotalSize = sizes.Sum()
            };
        }
    }
    public class Statistics
    {
        public string Path { get; set; }
        public long TotalSize { get; set; }
        public long MaxFileSize { get; set; }
        public long MinFileSize { get; set; }
        public double AverageFileSize { get; set; }
    }

}































































//var fileSizes = Directory.GetFiles(folderPath)
//.Select(path => new FileInfo(path))
//.Where(fileInfo => fileInfo.CreationTime < DateTime.Now.AddDays(-7))
//.OrderBy(fileInfo => fileInfo.LastAccessTime)
//.Select(fileInfo => fileInfo.Length)
//.TakeLast(10);

//return new Statistics
//{
//Path = folderPath,
//TotalSize = fileSizes.Sum(),
//MinFileSize = fileSizes.Min(),
//MaxFileSize = fileSizes.Max(),
//AverageFileSize = fileSizes.Average()
//};