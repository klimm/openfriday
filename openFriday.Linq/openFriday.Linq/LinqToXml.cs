﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace openFriday.Linq
{
    class LinqToXml
    {
        public static void XmlLinq()
        {
            string xmlStr = 
                "<root>" +
                    "<level test=\"A\" />" +
                    "<level test=\"A\" />" +
                    "<level test=\"B\" />" +
                    "<level test=\"B\" />" +
                    "<level test=\"C\" />" +
                "</root>";
            var xmlDocument = XDocument.Load(new StringReader(xmlStr));


            var bLevels = xmlDocument.Descendants("level")
                .Where(ele => ele.Attribute("test").Value == "B");
        }
    }
}
