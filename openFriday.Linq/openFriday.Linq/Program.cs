﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;

namespace openFriday.Linq
{
    class Program
    {
        static void Main()
        {
            var statsNormal = ComplexSample.GatherStatsNormal(Path.GetTempPath());
            ComplexSample.PrintStats(statsNormal);

            //var statsLinq = ComplexSample.GatherStatsLinq(Path.GetTempPath());
            //ComplexSample.PrintStats(statsLinq);

            //LinqToXml.XmlLinq();
        }

        private static int DescendingComparison(int x, int y)
        {
            return y.CompareTo(x);
        }
    }
}


//// Init
//int[] valuesNormal = new int[50];
//for (int i = 0; i < valuesNormal.Length; i++)
//{
//valuesNormal[i] = i + 100;
//}

//// Init-Linq
//int[] valuesLinq = Enumerable.Range(100, 50).ToArray();

//// Predicate
//var oddsNormal = new Collection<int>();
//foreach (int value in valuesNormal)
//{
//if(value % 2 != 0)
//oddsNormal.Add(value);
//}

//// Predicate-Linq
//var oddsLinq = valuesLinq.Where(value => value % 2 != 0).ToList();

//// Projection
//var binaryStringsNormal = new Collection<string>();
//foreach (int value in valuesNormal)
//{
//string strValue = Convert.ToString(value, 2);
//binaryStringsNormal.Add(strValue);
//}

//// Projection-Linq
//var binaryStringsLinq = valuesLinq.Select(value => Convert.ToString(value, 2));
            
//// Sort
//int[] sortedNormal = new int[valuesNormal.Length];
//Array.Copy(valuesNormal, sortedNormal, valuesNormal.Length);
//Array.Sort(sortedNormal, DescendingComparison);
            
//// Sort-Linq
//var sortedLinq = valuesLinq.OrderByDescending(value => value);


//// Helper
//int sumNormal = 0;
//foreach (var value in valuesNormal)
//{
//sumNormal += value;
//}
//int averageNormal = sumNormal / valuesNormal.Length;
            
//int[] first5 = new int[5];
//Array.Copy(valuesNormal, 0, first5, 0, 5);

//// Helper Linq
//var sum = valuesLinq.Sum();
//var average = valuesLinq.Average();
//var first5Linq = valuesLinq.Take(5);
